import requests as rq
import json


base_url = "https://we6.talentsprint.com/wordle/game/"
register_url = base_url + "register"
create_url = base_url + "create"
guess_url = base_url + "guess"


id = ''
session = rq.Session()


if not id:
    register_dict = {"mode": "Mastermind", "name": "Player"}
    reg = session.post(register_url, json=register_dict)
    response_data = reg.json()
    if "id" in response_data:
        id = response_data["id"]
    else:
        raise Exception("Failed to register player. Response: " + reg.text)


create_dict = {"id": id, "overwrite": True}
create_response = session.post(create_url, json=create_dict)





guess_dict = {"id": id, "guess": "hello"}
guess_response = session.post(guess_url, json=guess_dict)
print(guess_response.text)

